<?php

/**
 * Implements hook_rules_action_info().
 */
function sms_server_rules_action_info() {
  return array(
    'sms_server_comment_needs_approval_action' =>array(
      'label' => t('Send unapproved comment for approval'),
    ),

  );
}

