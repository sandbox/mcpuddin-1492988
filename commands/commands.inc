<?php

function sms_server_commands_commands() {
  return array(
    'commandlist' => array(
      'args'=>array(),
      'callback'=>'sms_server_commands_list',
    ),
  );
}

function sms_server_commands_list() {
  $commands = sms_server_get_command_list();
  $message = array();
  foreach($commands as $command_name=>$info) {
    $command = array($command_name);
    foreach($info['args'] as $arg_name) {
      $command[] = "[".$arg_name."]";
    }
    $message[] = implode(" ", $command);
  }

  return implode("\n", $message);
}