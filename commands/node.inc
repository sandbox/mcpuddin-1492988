<?php

function sms_server_node_commands() {
  return array(
    'node add' => array(
      'args'=>array(
          0=>'node type',
          1=>'node title'
       ),
      'callback'=>'sms_server_node_add',
     ),
    'node addu' => array(
      'args'=>array(
          0=>'node type',
          1=>'node title'
       ),
      'callback'=>'sms_server_node_addu',
    ),
  );
}

function sms_server_node_add($node_type, $title) {
  $node = new stdClass();
  //@todo type validation
  $node->type = $node_type;
  $node->title = $title;
  $node->status = 1;
  node_object_prepare($node);
  node_save($node);
  return "Created published ".$node_type;
}

function sms_server_node_addu($node_type, $title) {
  $node = new stdClass();
  //@todo type validation
  $node->type = $node_type;
  $node->title = $title;
  $node->status = 0;
  node_object_prepare($node);
  node_save($node);
  return "Created unpublished ".$node_type;
}
