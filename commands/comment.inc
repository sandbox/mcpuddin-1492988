<?php

function sms_server_comment_commands() {
  return array(
    'commentapprove' => array(
      'args'=>array(0=>'cid'),
      'callback'=>'sms_server_comment_approve',
    ),
  );
}

function sms_server_comment_approve($cid) {
  $comment = comment_load($cid);
  if(!$comment->cid) {
    return "Invalid comment id";
  }

  $comment->status = 1;
  comment_save($comment);

  return "Comment ".$cid." approved";
  
}

function sms_server_comment_list() {
  return 'list';
}

function sms_server_comment_needs_approval_action($object, $context) {
  //@TODO this comment should be mandatory and only apply to comment insertions

  $comment = $context->variables['comment']->value(array());
  $message = "Reply 'commentapprove $comment->cid' : ".$comment->subject;

  $numbers = _sms_get_all_confirmed_numbers();
  foreach($numbers as $number) {
    //@TODO do not hardcode the phoen number
    sms_server_log("Sending comment message to $number ");
    sms_send($number, $message);
  }

}